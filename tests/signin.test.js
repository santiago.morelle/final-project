import { signInPath } from "../data/paths";
import { validAccounts } from "../data/accounts";
import SignInPage from "../pages/signin.page";
import AccountPage from "../pages/account.page";

describe("Sign In", () => {
  validAccounts.forEach((account) => {
    it("should sign in with valid credentials when sign in button is pressed", async () => {
      await SignInPage.open(signInPath);
      await SignInPage.signIn(account.emailAddress, account.password);
      await expect(AccountPage.myAccountTitle).toExist();
      await browser.deleteCookies();
    });
  });
});

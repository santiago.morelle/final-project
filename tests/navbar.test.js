import HomePage from "../pages/home.page";

describe("Navbar", () => {
  it("should compare successful with a baseline", async () => {
    await HomePage.open("https://www.demoblaze.com/index.html");
    await expect(await browser.checkElement(await HomePage.navbar)).toEqual(0);
  });
});

import HomePage from "../pages/home.page";
import SignUpPage from "../pages/signup.page";

describe("Sign Up", () => {
  it("should sign up with valid credentials when sign up button is pressed", async () => {
    await HomePage.open("https://www.demoblaze.com/index.html");
    await HomePage.clickElement(HomePage.signUpLink);
    await SignUpPage.signUp("santiagomorelle", "123456");
    await expect(SignUpPage.signUpModal).not.toBeDisplayed();
  });
});

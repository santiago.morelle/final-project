import HomePage from "../pages/home.page";
import ProductPage from "../pages/product.page";

describe("Product Detail", () => {
  it("should show the details of the selected product", async () => {
    await HomePage.open("https://www.demoblaze.com/index.html");
    await HomePage.clickElement(HomePage.nexusDiv);
    await expect(ProductPage.productTitle).toHaveTextContaining("Nexus 6");
  });
});

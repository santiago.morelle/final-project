import HomePage from "../pages/home.page";
import ProductPage from "../pages/product.page";
import CartPage from "../pages/cart.page";

describe("Cart", () => {
  it("should add the selected product to the cart", async () => {
    await HomePage.open("https://www.demoblaze.com/index.html");
    await HomePage.clickElement(HomePage.nexusDiv);
    await ProductPage.clickElement(ProductPage.addToCartButton);
    await HomePage.clickElement(HomePage.cartLink);
    await expect(CartPage.nexusTd).toExist();
  });
});

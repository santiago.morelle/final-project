import BasePage from "./base.page";

class ProductPage extends BasePage {
  get productTitle() {
    return $(".name");
  }

  get addToCartButton() {
    return $("a=Add to cart");
  }
}

export default new ProductPage();

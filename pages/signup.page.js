import BasePage from "./base.page";

class SignUpPage extends BasePage {
  get signUpModal() {
    return $("#signInModal");
  }

  get usernameTextBox() {
    return $("#sign-username");
  }

  get passwordTextBox() {
    return $("#sign-password");
  }

  get signUpButton() {
    return $("button=Sign up");
  }

  async signUp(username, password) {
    await this.completeCredentials(username, password);
    await this.clickSignUpButton();
  }

  async completeCredentials(username, password) {
    await super.completeFieldWithText(await this.usernameTextBox, username);
    await super.completeFieldWithText(await this.passwordTextBox, password);
  }

  async clickSignUpButton() {
    await super.clickElement(await this.signUpButton);
  }
}

export default new SignUpPage();

import BasePage from "./base.page";

class AccountPage extends BasePage {
  get myAccountTitle() {
    return $("h1=My account");
  }
}

export default new AccountPage();

import BasePage from "./base.page";

class HomePage extends BasePage {
  get navbar() {
    return $("#navbarExample");
  }

  get cartLink() {
    return $("#cartur");
  }

  get signUpLink() {
    return $("#signin2");
  }

  get nexusDiv() {
    return $("a=Nexus 6");
  }
}

export default new HomePage();

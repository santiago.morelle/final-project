import BasePage from "./base.page";

class CartPage extends BasePage {
  get nexusTd() {
    return $("td=Nexus 6");
  }
}

export default new CartPage();
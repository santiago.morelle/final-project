import BasePage from "./base.page";

class SignInPage extends BasePage {
  get emailAddressTextBox() {
    return $('[id="email"]');
  }

  get passwordTextBox() {
    return $('[id="passwd"]');
  }

  get signInButton() {
    return $('[id="SubmitLogin"]');
  }

  /**
   * Completes email address and password fields and signs in
   * @param {String} emailAddress to fill email address field
   * @param {String} password to fill password field
   */
  async signIn(emailAddress, password) {
    await this.completeCredentials(emailAddress, password);
    await this.clickSignInButton();
  }

  /**
   * Completes email address and password fields
   * @param {String} emailAddress to fill email address field
   * @param {String} password to fill password field
   */
  async completeCredentials(emailAddress, password) {
    await super.completeFieldWithText(
      await this.emailAddressTextBox,
      emailAddress
    );
    await super.completeFieldWithText(await this.passwordTextBox, password);
  }

  async clickSignInButton() {
    await super.clickElement(await this.signInButton);
  }
}

export default new SignInPage();
